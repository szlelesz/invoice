--------------------------------------------------------
--  File created - Wednesday-January-20-2016   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body CRANBURY
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "SYS"."CRANBURY" AS
  

    
  PROCEDURE DATAINSERT AS
  BEGIN        
      RPT_INV_SUMM_HUN_STAGING();
      INASN();
  END;
  
  PROCEDURE RPT_INV_SUMM_HUN_STAGING AS
      v_code NUMBER;
     v_errm VARCHAR2(64);
  BEGIN
  SAVEPOINT "START";
  INSERT INTO RPT_INV_SUMM_HUN_STAGING@CRANBURY 
    SELECT 'HU004' AS Site_code,t.phy_attrib_11 AS Item_code_NDC11, s.Lot_no,SUM(CASE when l.inv_stat <> 'SALE' then s.quantity ELSE 0 END) AS Quarantine_qty, 
    SUM(CASE when l.inv_stat = 'SALE' then s.quantity ELSE 0 END) AS Qty, NVL(d.Trans_qty,0) AS In_transit,d.Ship_date AS Ship_date, MAX(s.exp_date) AS EXPIRE_DATE,              -----EXPIRE_DATE EXP_DATE
    TRUNC(SYSDATE) AS Process_date                  
    FROM stock@ITM2LIVE s
      LEFT OUTER JOIN item@ITM2LIVE t ON s.item_code = t.item_code
      LEFT OUTER JOIN LOCATION@ITM2LIVE l ON l.loc_code = s.loc_code
    LEFT OUTER JOIN
      (SELECT i.item_code,i.lot_no,(-1)*SUM(i.eff_qty) AS Trans_qty,MAX(i.tran_date) AS Ship_date
      FROM invtrace@ITM2LIVE i
      LEFT OUTER JOIN item@ITM2LIVE t ON i.item_code = t.item_code
      WHERE i.ref_ser in ('S-DSP','S-RET') AND t.Phy_attrib_2 = 'Manufactured By'
      GROUP BY i.item_code, i.lot_no
      HAVING MAX(i.tran_date) > SYSDATE-30
      ) d  ON d.item_code = s.item_code AND d.lot_no = s.lot_no
    WHERE ((s.quantity >0 AND l.inv_stat <> 'REJ'  AND s.loc_code not like '29%' ) OR d.lot_no IS NOT NULL) 
      AND t.Phy_attrib_2 = 'Manufactured By'
    GROUP BY 'HU004', t.phy_attrib_11, s.Lot_no, nvl(d.Trans_qty,0), d.Ship_date, TRUNC(SYSDATE)    ---2016.01.07.
    HAVING MAX(s.exp_date) > SYSDATE ;
  INSERT INTO PORTAL.SCHEDULE_LOG(SCHEDULE) VALUES('CRANBURY');
  COMMIT;
    EXCEPTION 
      WHEN OTHERS THEN
        ROLLBACK TO "START";
        --RAISE_APPLICATION_ERROR(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
        v_code := SQLCODE;
        v_errm := SUBSTR(SQLERRM, 1 , 64);
        INSERT INTO PORTAL.SCHEDULE_LOG(SCHEDULE,CODE,MESSAGE) VALUES('CRANBURY',v_code,v_errm);        
        END;

PROCEDURE INASN AS
    v_code NUMBER;
     v_errm VARCHAR2(64);
BEGIN
  SAVEPOINT "START";
  INSERT INTO erp.inasn@CRANBURY
    SELECT despatch.desp_id AS shpn,despatchdet.gross_weight AS wghtc,substr(despatch.trans_mode, 1, 1) AS tmode,invoice.invoice_id AS inv,
      to_char(invoice.conf_date, 'YYYYMMDD') AS shdt,'SF' AS sfqal,substr(site.sh_descr, 1, 30) AS sfnm, '1100012707' AS sfgln,substr(site.add1, 1, 30) AS sfadd1,
      substr(site.add2, 1, 30) AS sfadd2,substr(site.city, 1, 30) AS sfcty,site.pin AS sfpost,substr(state.count_code, 1, 3) AS sfcou,'ST' AS stqal,
      substr(customer.cust_name, 1, 30) AS stnm,NULL AS stgln, substr(trim(despatch.dlv_add1)||' '||trim(despatch.dlv_add2), 1, 30) AS stadd1,
      despatch.dlv_city AS stcty, substr(customer.state_code, 1, 2) AS stste,despatch.dlv_pin AS STPOST,substr(despatch.count_code__dlv, 1, 3) AS stcou,
      '$' AS palt,despatchdet.lot_sl AS PLTNO,'VN' AS vnqal,substr(item.item_code__ndc, 1, 14) AS item,'LT' AS ltqal,substr(despatchdet.lot_no, 1, 10) AS lot,
      despatchdet.quantity  AS QTY,despatchdet.unit AS uom,item_lot_packsize.shipper_size  AS INQT,to_char(despatchdet.exp_date, 'YYYYMMDD') AS exp, 'N' AS asn_flag
    FROM invoice@ITM2LIVE, despatch@ITM2LIVE, despatchdet@ITM2LIVE, site@ITM2LIVE, state@ITM2LIVE, item@ITM2LIVE, customer@ITM2LIVE, item_lot_packsize@ITM2LIVE
    WHERE  TRUNC(invoice.conf_date)  =   TRUNC(SYSDATE)
      AND invoice.confirmed  =  'Y' 
      AND invoice.desp_id  =  despatch.desp_id
      AND despatch.count_code__dlv  =  'USA'
      AND despatchdet.desp_id  =  despatch.desp_id
      AND site.site_code  =  despatch.site_code
      AND state.state_code  =  site.state_code
      AND item.item_code  =  despatchdet.item_code
      AND customer.cust_code  =  despatch.cust_code__dlv
      AND item_lot_packsize.item_code  =  despatchdet.item_code
      AND despatchdet.lot_no between item_lot_packsize.lot_no__FROM 
      AND item_lot_packsize.lot_no__to;      
  INSERT INTO PORTAL.SCHEDULE_LOG(SCHEDULE) VALUES('CRANBURY');
  COMMIT;
    EXCEPTION 
      WHEN OTHERS THEN
        ROLLBACK TO "START";
        v_code := SQLCODE;
        v_errm := SUBSTR(SQLERRM, 1 , 64);
        INSERT INTO PORTAL.SCHEDULE_LOG(SCHEDULE,CODE,MESSAGE) VALUES('CRANBURY',v_code,v_errm);        
        --RAISE_APPLICATION_ERROR(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
END;

END CRANBURY;

/
