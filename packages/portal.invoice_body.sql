--------------------------------------------------------
--  File created - Wednesday-January-20-2016   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body INVOICE
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "PORTAL"."INVOICE" 
AS
PROCEDURE INSERT_INLAND_INVOICE_BODY(
    V_INVOICE_ID VARCHAR2)
AS
  INVOICE_BODY PORTAL.INVOICE_BODY%ROWTYPE;
  CURSOR BODY_CUR
  IS
    SELECT NULL as ID,NVL(sr.tran_id,it.invoice_id) as invoice_id,sr.invoice_id as hiv_id,it.item_code,i.item_code__ndc vtsz,i.descr,SUM(it.quantity) quantity,
      u.udf_str1,it.curr_code,it.rate,der.exch_rate,SUM((it.net_amt-it.tax_amt)) without_vat,NVL(tt.tax_perc,'0') tax_env,SUM(it.tax_amt) tax_amt,SUM(it.net_amt) net_amt
    FROM invoice_trace@ITM2LIVE it
    LEFT OUTER JOIN item@ITM2LIVE i ON i.item_code=it.item_code
    LEFT OUTER JOIN invoice@ITM2LIVE inv ON inv.invoice_id=it.invoice_id
    LEFT OUTER JOIN uom@ITM2LIVE u ON u.unit=it.unit
    LEFT OUTER JOIN taxtran@ITM2LIVE tt ON (tt.tran_id =it.invoice_id AND tt.line_no =it.inv_line_no)
    LEFT OUTER JOIN daily_exch_rate_sell_buy@ITM2LIVE der ON der.from_date=inv.desp_date AND der.curr_code =inv.curr_code AND der.curr_code__to ='HUF'
    LEFT OUTER JOIN sreturn@ITM2LIVE sr ON sr.invoice_id=inv.invoice_id AND sr.full_ret ='Y'
    WHERE it.invoice_id = V_INVOICE_ID
    GROUP BY sr.invoice_id, NVL(sr.tran_id,it.invoice_id), it.item_code,i.item_code__ndc,i.descr,u.udf_str1,it.curr_code,it.rate,der.exch_rate,NVL(tt.tax_perc,'0');
BEGIN
  OPEN BODY_CUR;
   
   LOOP
      
      FETCH BODY_CUR INTO INVOICE_BODY;      
      EXIT WHEN BODY_CUR%NOTFOUND;          
      INSERT INTO PORTAL.INVOICE_BODY(ID, INVOICE_ID, HIV_ID,ITEM_CODE,VTSZ,DESCR,QUANTITY,UDF_STR2,CURR_CODE,RATE,EXCH_RATE,WITHOUT_VAT,TAX_ENV,TAX_AMT,NET_AMT)
      VALUES (INVOICE_BODY.ID, INVOICE_BODY.INVOICE_ID,INVOICE_BODY.HIV_ID,INVOICE_BODY.ITEM_CODE,INVOICE_BODY.VTSZ,INVOICE_BODY.DESCR,INVOICE_BODY.QUANTITY,INVOICE_BODY.UDF_STR2,INVOICE_BODY.CURR_CODE,INVOICE_BODY.RATE,INVOICE_BODY.EXCH_RATE,INVOICE_BODY.WITHOUT_VAT,INVOICE_BODY.TAX_ENV,INVOICE_BODY.TAX_AMT,INVOICE_BODY.NET_AMT);
    END LOOP;
    CLOSE BODY_CUR;

END;
PROCEDURE INSERT_INLAND_INVOCES AS
  INVOICE_HEAD PORTAL.INVOICE_HEAD%ROWTYPE;
  CURSOR HEAD_CUR
  IS
  SELECT NULL,NVL(sr.tran_id,i.invoice_id) invoice_id, sr.invoice_id hiv_id,(SELECT iis.type FROM invoice_itemsers iis WHERE iis.item_ser =  LTRIM(RTRIM(i.item_ser))) AS invoice_type,
	(SELECT DISTINCT(sgrp_code) FROM item@ITM2LIVE WHERE item_code IN (SELECT item_code FROM invdet@ITM2LIVE WHERE invoice_id=i.invoice_id ) AND sgrp_code='TD000') AS sgrp_code,
	(SELECT COUNT(phy_attrib_4) FROM item@ITM2LIVE WHERE item_code IN   (SELECT item_code FROM invdet@ITM2LIVE WHERE invoice_id=i.invoice_id) AND phy_attrib_4='Y') AS phy_attrib_4,
	s.udf1 site, NULL AS country, NULL AS eu_vat_no, s.pin,s.city, s.add1,si.reg_no TIN, b.long_name, NULL AS bank_pin,NULL AS bank_city,
	NULL AS bank_addr1, NULL AS bank_state, b.udf_str1 ||'-' ||b.udf_str2 ||'-' ||b.udf_str3 bank_acc, NULL AS bank_swift, u.name,
	s.tele1, so.cust_code__dlv sold_to,c1.full_name sold_name, c1.addr1 sold_addr,NULL AS sold_addr2, NULL AS sold_addr3,
	c1.pin sold_pin, c1.city sold_city, NULL AS sold_state, NULL AS sold_region, so.cust_code__bil customer, c.full_name cust_name, NULL AS full_name, i.CUST_CODE__BIL cust_code,
	c.addr1 cust_addr, NULL AS cust_addr2, NULL AS cust_addr3, c.pin cust_pin, c.city cust_city, NULL AS cust_state, c.lst_no cust_eu_vat, NULL AS trans_mode, NULL AS cust_parity,
	so.cust_pord order_no, NULL order_date, so.sale_order export_contract_no, i.desp_date, d.conf_date invoice_date, i.due_date, 
	(CASE so.rcp_mode   WHEN 'T'   THEN '�tutal�s'   WHEN 'S'   THEN 'K�szp�nz'   WHEN 'C'   THEN 'K�szp�nz'   WHEN 'E'   THEN '�tutal�s'   WHEN 'Q'   THEN 'Csekk' END) rcp_mode,
	so.cr_term, NULL AS term_of_delivery, i.item_ser, der.exch_rate, i.curr_code, i.net_amt, i.tax_amt, NULL AS without_vat, NULL AS destination, com.comm_text remark, NULL AS comm_inv
FROM invoice@ITM2LIVE i LEFT OUTER JOIN users@ITM2LIVE u ON i.chg_user=u.code
LEFT OUTER JOIN site@ITM2LIVE s    ON s.site_code=i.site_code    
LEFT OUTER JOIN state@ITM2LIVE st    ON s.state_code=st.state_code
    LEFT OUTER JOIN siteregno@ITM2LIVE si    ON si.site_code=s.site_code    AND si.ref_code='CST/001'
    LEFT OUTER JOIN siteregno@ITM2LIVE si2    ON si2.site_code=s.site_code    AND si2.ref_code='LST/001'
    LEFT OUTER JOIN despatch@ITM2LIVE d    ON i.desp_id=d.desp_id
    LEFT OUTER JOIN sorder@ITM2LIVE so    ON so.sale_order=i.sale_order
    LEFT OUTER JOIN customer@ITM2LIVE c    ON c.cust_code=so.cust_code__bil
    LEFT OUTER JOIN country@ITM2LIVE st1    ON c.count_code=st1.count_code
    LEFT OUTER JOIN bank@ITM2LIVE b    ON b.bank_code=i.bank_code
    LEFT OUTER JOIN country@ITM2LIVE st2    ON b.count_code=st2.count_code
    LEFT OUTER JOIN customer@ITM2LIVE c1    ON c1.cust_code=so.cust_code__dlv
    LEFT OUTER JOIN country@ITM2LIVE stx    ON c1.count_code=stx.count_code
    LEFT OUTER JOIN comments@ITM2LIVE com    ON com.ref_id =i.invoice_id
    LEFT OUTER JOIN daily_exch_rate_sell_buy@ITM2LIVE der    ON der.from_date=i.desp_date    AND der.curr_code    =i.curr_code    AND der.curr_code__to='HUF'
    LEFT OUTER JOIN sreturn@itm2live sr    ON sr.invoice_id   =i.invoice_id    AND sr.full_ret    ='Y'
    WHERE NVL(sr.tran_id,i.invoice_id) NOT IN (SELECT INVOICE_ID FROM INVOICE_HEAD ) AND d.conf_date IS NOT NULL 
    AND (SELECT iis.type FROM invoice_itemsers iis WHERE iis.item_ser =  LTRIM(RTRIM(i.item_ser))) <> 'E' AND d.conf_date > ADD_MONTHS(sysdate,-1);
  BEGIN
    OPEN HEAD_CUR;
    LOOP
      FETCH HEAD_CUR INTO INVOICE_HEAD;
      EXIT
    WHEN HEAD_CUR%NOTFOUND;
      INSERT INTO PORTAL.INVOICE_HEAD(ID, INVOICE_ID,HIV_ID,INVOICE_TYPE,SRGP_CODE,PHY_ATTRIB_4,SITE,COUNTRY,EU_VAT_NO,PIN,CITY,ADD1,TIN,LONG_NAME,BANK_PIN,BANK_CITY,BANK_ADDR1,BANK_STATE,BANK_ACC,BANK_SWIFT,NAME,
          TELE1,SOLD_TO,SOLD_NAME,SOLD_ADDR,SOLD_ADDR2,SOLD_ADDR3,SOLD_PIN,SOLD_CITY,SOLD_STATE,SOLD_REGION,CUSTOMER,CUST_NAME,FULL_NAME,CUST_CODE,CUST_ADDR,CUST_ADDR2,CUST_ADDR3,
          CUST_PIN,CUST_CITY,CUST_STATE,CUST_EU_VAT,TRANS_MODE,CUST_PARITY,ORDER_NO,ORDER_DATE,EXPORT_CONTRACT_NO,DESP_DATE,INVOICE_DATE,DUE_DATE,RCP_MODE,CR_TERM,TERMS_OF_DELIVERY,ITEM_SER,
          EXCH_RATE,CURR_CODE,NET_AMT,TAX_AMT,WITHOUT_VAT,DESTINATION,REMARK,COMM_INV)
        VALUES(
          INVOICE_HEAD.ID,INVOICE_HEAD.INVOICE_ID,INVOICE_HEAD.HIV_ID,INVOICE_HEAD.INVOICE_TYPE,INVOICE_HEAD.SRGP_CODE,INVOICE_HEAD.PHY_ATTRIB_4,INVOICE_HEAD.SITE,INVOICE_HEAD.COUNTRY,INVOICE_HEAD.EU_VAT_NO,INVOICE_HEAD.PIN,
          INVOICE_HEAD.CITY,INVOICE_HEAD.ADD1,INVOICE_HEAD.TIN,INVOICE_HEAD.LONG_NAME,INVOICE_HEAD.BANK_PIN,INVOICE_HEAD.BANK_CITY,INVOICE_HEAD.BANK_ADDR1,INVOICE_HEAD.BANK_STATE,INVOICE_HEAD.BANK_ACC,
          INVOICE_HEAD.BANK_SWIFT,INVOICE_HEAD. NAME,INVOICE_HEAD.TELE1,INVOICE_HEAD.SOLD_TO,INVOICE_HEAD.SOLD_NAME,INVOICE_HEAD.SOLD_ADDR,INVOICE_HEAD.SOLD_ADDR2,INVOICE_HEAD.SOLD_ADDR3,INVOICE_HEAD.SOLD_PIN,
          INVOICE_HEAD.SOLD_CITY,INVOICE_HEAD.SOLD_STATE,INVOICE_HEAD.SOLD_REGION,INVOICE_HEAD.CUSTOMER,INVOICE_HEAD.CUST_NAME,INVOICE_HEAD.FULL_NAME,INVOICE_HEAD.CUST_CODE,INVOICE_HEAD.CUST_ADDR,
          INVOICE_HEAD.CUST_ADDR2,INVOICE_HEAD.CUST_ADDR3,INVOICE_HEAD.CUST_PIN,INVOICE_HEAD.CUST_CITY,INVOICE_HEAD.CUST_STATE,INVOICE_HEAD.CUST_EU_VAT,INVOICE_HEAD.TRANS_MODE,INVOICE_HEAD.CUST_PARITY,
          INVOICE_HEAD.ORDER_NO,INVOICE_HEAD.ORDER_DATE,INVOICE_HEAD.EXPORT_CONTRACT_NO,INVOICE_HEAD. DESP_DATE,INVOICE_HEAD.INVOICE_DATE,INVOICE_HEAD.DUE_DATE,INVOICE_HEAD.RCP_MODE,INVOICE_HEAD.CR_TERM,
          INVOICE_HEAD.TERMS_OF_DELIVERY,INVOICE_HEAD.ITEM_SER,INVOICE_HEAD.EXCH_RATE,INVOICE_HEAD.CURR_CODE,INVOICE_HEAD.NET_AMT,INVOICE_HEAD.TAX_AMT,INVOICE_HEAD.WITHOUT_VAT,INVOICE_HEAD.DESTINATION,
          INVOICE_HEAD.REMARK,INVOICE_HEAD.COMM_INV
      );      
      INSERT_INLAND_INVOICE_BODY(INVOICE_HEAD.INVOICE_ID);      
    END LOOP;
    CLOSE HEAD_CUR;
  END;
          
PROCEDURE INSERT_EXPORT_INVOICE_BODY(V_INVOICE_ID VARCHAR2)
AS
  INVOICE_BODY PORTAL.INVOICE_BODY%ROWTYPE;
  CURSOR BODY_CUR
  IS
  SELECT NULL as ID,NVL(sr.tran_id,it.invoice_id) invoice_id,sr.invoice_id hiv_id, it.item_code,i.item_code__ndc vtsz,i.tech_descr,SUM(it.quantity) quantity,
    u.udf_str2,it.curr_code,it.rate,der.exch_rate,SUM((it.net_amt-it.tax_amt)) without_vat,NVL(tt.tax_perc,'0') tax_env,SUM(it.tax_amt) tax_amt,
    SUM(it.net_amt) net_amt
  FROM invoice_trace@ITM2LIVE it
  LEFT OUTER JOIN item@ITM2LIVE i ON i.item_code =it.item_code
  LEFT OUTER JOIN invoice@ITM2LIVE inv ON inv.invoice_id=it.invoice_id
  LEFT OUTER JOIN uom@ITM2LIVE u ON u.unit=it.unit
  LEFT OUTER JOIN sreturn@ITM2LIVE sr ON sr.invoice_id =inv.invoice_id AND sr.full_ret='Y'
  LEFT OUTER JOIN daily_exch_rate_sell_buy@ITM2LIVE der ON der.from_date =inv.desp_date AND der.curr_code=inv.curr_code AND der.curr_code__to='HUF'
  LEFT OUTER JOIN taxtran@ITM2LIVE tt ON (tt.tran_id =it.invoice_id AND tt.line_no__tax=it.inv_line_no)
  WHERE it.invoice_id  = V_INVOICE_ID
  GROUP BY sr.invoice_id,NVL(sr.tran_id,it.invoice_id),it.invoice_id,it.item_code,i.item_code__ndc,i.tech_descr,u.udf_str2,
    it.curr_code,it.rate,der.exch_rate,NVL(tt.tax_perc,'0');
BEGIN
  OPEN BODY_CUR;
  --DBMS_OUTPUT.PUT_LINE('--->' || V_INVOICE_ID);
   LOOP
      FETCH BODY_CUR INTO INVOICE_BODY;      
      EXIT WHEN BODY_CUR%NOTFOUND;
      /*DBMS_OUTPUT.PUT_LINE(':' || INVOICE_BODY.INVOICE_ID);
      DBMS_OUTPUT.PUT_LINE(':' || INVOICE_BODY.ITEM_CODE);      
      DBMS_OUTPUT.PUT_LINE(':' || INVOICE_BODY.DESCR);*/
      
      INSERT INTO PORTAL.INVOICE_BODY(ID, INVOICE_ID, HIV_ID,ITEM_CODE,VTSZ,DESCR,QUANTITY,UDF_STR2,CURR_CODE,RATE,EXCH_RATE,WITHOUT_VAT,TAX_ENV,TAX_AMT,NET_AMT)
      VALUES (INVOICE_BODY.ID, INVOICE_BODY.INVOICE_ID,INVOICE_BODY.HIV_ID,INVOICE_BODY.ITEM_CODE,INVOICE_BODY.VTSZ,INVOICE_BODY.DESCR,INVOICE_BODY.QUANTITY,INVOICE_BODY.UDF_STR2,INVOICE_BODY.CURR_CODE,INVOICE_BODY.RATE,INVOICE_BODY.EXCH_RATE,INVOICE_BODY.WITHOUT_VAT,INVOICE_BODY.TAX_ENV,INVOICE_BODY.TAX_AMT,INVOICE_BODY.NET_AMT);
    END LOOP;
    CLOSE BODY_CUR;  
END;


PROCEDURE INSERT_EXPORT_INVOCES
AS
  INVOICE_HEAD PORTAL.INVOICE_HEAD%ROWTYPE;
  CURSOR HEAD_CUR
  IS
  SELECT NULL AS id,x.invoice_id,x.hiv_id,'E' AS invoice_type, NULL AS srgp_code,NULL AS phy_attrib_4,x.site,x.country,x.EU_VAT_no,x.pin,x.city,x.add1,x.TIN,x.long_name,x.bank_pin,x.bank_city,
    x.bank_addr1,x.bank_state,x.bank_acc,x.bank_swift,NULL AS name,NULL AS tele1,x.sold_to,c1.full_name sold_name,c1.addr1 sold_addr,c1.addr2 sold_addr2,c1.addr3 sold_addr3,
    c1.pin sold_pin,c1.city sold_city,stx.descr sold_state,stx.region sold_region,x.customer,c.full_name AS cust_name,NULL AS full_name,NULL AS cust_code,c.addr1 cust_addr,
    c.addr2 cust_addr2,c.addr3 cust_addr3,c.pin cust_pin,c.city cust_city,st1.descr cust_state,c.lst_no cust_eu_vat,x.trans_mode,x.cust_parity,x.order_no,x.order_date,
    x.export_contract_no,x.desp_date,x.invoice_date,x.due_date,x.rcp_mode,x.cr_term,x.terms_of_delivery,NULL AS item_ser,x.exch_rate,NULL AS curr_code,SUM(x.net_amt) net_amt,
    SUM(x.tax_amt) tax_amt,SUM(x.without_vat) without_vat,x.destination,NULL AS REMARK,
    x.comm_inv 
    FROM
      (SELECT NVL(sr.tran_id,i.invoice_id) invoice_id,sr.invoice_id hiv_id,s.udf2 site,s.pin,s.city,s.add1,st.descr country,si.reg_no TIN,
      si2.reg_no EU_VAT_no,so.cust_code__dlv sold_to,der.exch_rate,(it.net_amt-it.tax_amt) without_vat,(it.tax_amt) tax_amt,(it.net_amt) net_amt,
      d.conf_date invoice_date,so.cust_pord order_no,so.pord_date order_date,so.sale_order export_contract_no,so.cust_code__bil customer,
      (CASE d.trans_mode WHEN 'A'THEN 'By Air' WHEN 'R' THEN 'By Road' WHEN 'S' THEN 'By Ship'END) trans_mode,
      so.dlv_term cust_parity,b.long_name,b.pin bank_pin,b.city bank_city,b.addr1 bank_addr1,st2.descr bank_state,
      b.udf_str4 ||'-' ||b.udf_str1 ||'-' ||b.udf_str2 ||'-' ||b.udf_str3 bank_acc,
      b.swift_code bank_swift,d.desp_date,i.due_date,
      (CASE so.rcp_mode WHEN 'T' THEN 'Transfer' WHEN 'S' THEN 'Cash' WHEN 'E' THEN 'Transfer' WHEN 'Q' THEN 'Cheque' END) rcp_mode,
      so.cr_term,d.udf__str1 AS destination,d.udf__str2 terms_of_delivery,d.sb_no comm_inv
  FROM invoice@ITM2LIVE i
    LEFT OUTER JOIN site@ITM2LIVE s ON s.site_code=i.site_code
    LEFT OUTER JOIN sreturn@ITM2LIVE sr ON sr.invoice_id =i.invoice_id AND sr.full_ret='Y'
    LEFT OUTER JOIN invoice_trace@ITM2LIVE it ON it.invoice_id=i.invoice_id
    LEFT OUTER JOIN state@ITM2LIVE st ON s.state_code=st.state_code
    LEFT OUTER JOIN siteregno@ITM2LIVE si ON si.site_code=s.site_code AND si.ref_code='CST/001'
    LEFT OUTER JOIN siteregno@ITM2LIVE si2 ON si2.site_code=s.site_code AND si2.ref_code='LST/001'
    LEFT OUTER JOIN despatch@ITM2LIVE d ON i.desp_id=d.desp_id
    LEFT OUTER JOIN sorder@ITM2LIVE so ON so.sale_order=i.sale_order
    LEFT OUTER JOIN daily_exch_rate_sell_buy@ITM2LIVE der ON der.from_date =i.desp_date AND der.curr_code=i.curr_code AND der.curr_code__to='HUF'
    LEFT OUTER JOIN bank@ITM2LIVE b ON b.bank_code=i.bank_code
    LEFT OUTER JOIN country@ITM2LIVE st2 ON b.count_code=st2.count_code
    LEFT OUTER JOIN customer@ITM2LIVE cy ON cy.cust_code =so.cust_code__dlv WHERE LTRIM(RTRIM(i.item_ser)) IN (SELECT iis.item_ser FROM invoice_itemsers iis WHERE iis.type ='E')) x
    LEFT OUTER JOIN customer@ITM2LIVE c ON c.cust_code=x.customer
    LEFT OUTER JOIN country@ITM2LIVE st1 ON c.count_code=st1.count_code
    LEFT OUTER JOIN customer@ITM2LIVE c1 ON c1.cust_code=x.sold_to
    LEFT OUTER JOIN country@ITM2LIVE stx ON c1.count_code=stx.count_code WHERE invoice_id NOT IN (SELECT INVOICE_ID FROM INVOICE_HEAD) AND invoice_date > ADD_MONTHS(sysdate,-1)
  GROUP BY x.invoice_id, x.hiv_id, x.site, x.pin, x.city, x.add1, x.country, x.TIN, x.EU_VAT_no,
    x.sold_to, c1.full_name, c1.addr1, c1.addr2, c1.addr3, c1.pin, c1.city, stx.descr, x.exch_rate,
    x.invoice_date, x.order_no, x.order_date, x.export_contract_no, x.customer, c.full_name,
    c.addr1, c.addr2, c.addr3, c.pin, c.city, st1.descr, c.lst_no, x.trans_mode,x.cust_parity,
    x.long_name, x.bank_pin,x.bank_city, x.bank_addr1, x.bank_state, x.bank_acc, x.bank_swift,
    x.desp_date,x.due_date,x.rcp_mode,stx.region,x.cr_term,x.destination,x.terms_of_delivery,x.comm_inv;
BEGIN
  OPEN HEAD_CUR;
    LOOP
      FETCH HEAD_CUR INTO INVOICE_HEAD;
      EXIT
    WHEN HEAD_CUR%NOTFOUND;
    INSERT INTO PORTAL.INVOICE_HEAD(ID, INVOICE_ID,HIV_ID,INVOICE_TYPE,SRGP_CODE,PHY_ATTRIB_4,SITE,COUNTRY,EU_VAT_NO,PIN,CITY,ADD1,TIN,LONG_NAME,BANK_PIN,BANK_CITY,BANK_ADDR1,BANK_STATE,BANK_ACC,BANK_SWIFT,NAME,
          TELE1,SOLD_TO,SOLD_NAME,SOLD_ADDR,SOLD_ADDR2,SOLD_ADDR3,SOLD_PIN,SOLD_CITY,SOLD_STATE,SOLD_REGION,CUSTOMER,CUST_NAME,FULL_NAME,CUST_CODE,CUST_ADDR,CUST_ADDR2,CUST_ADDR3,
          CUST_PIN,CUST_CITY,CUST_STATE,CUST_EU_VAT,TRANS_MODE,CUST_PARITY,ORDER_NO,ORDER_DATE,EXPORT_CONTRACT_NO,DESP_DATE,INVOICE_DATE,DUE_DATE,RCP_MODE,CR_TERM,TERMS_OF_DELIVERY,ITEM_SER,
          EXCH_RATE,CURR_CODE,NET_AMT,TAX_AMT,WITHOUT_VAT,DESTINATION,REMARK,COMM_INV)
        VALUES(
          INVOICE_HEAD.ID,INVOICE_HEAD.INVOICE_ID,INVOICE_HEAD.HIV_ID,INVOICE_HEAD.INVOICE_TYPE,INVOICE_HEAD.SRGP_CODE,INVOICE_HEAD.PHY_ATTRIB_4,INVOICE_HEAD.SITE,INVOICE_HEAD.COUNTRY,INVOICE_HEAD.EU_VAT_NO,INVOICE_HEAD.PIN,
          INVOICE_HEAD.CITY,INVOICE_HEAD.ADD1,INVOICE_HEAD.TIN,INVOICE_HEAD.LONG_NAME,INVOICE_HEAD.BANK_PIN,INVOICE_HEAD.BANK_CITY,INVOICE_HEAD.BANK_ADDR1,INVOICE_HEAD.BANK_STATE,INVOICE_HEAD.BANK_ACC,
          INVOICE_HEAD.BANK_SWIFT,INVOICE_HEAD. NAME,INVOICE_HEAD.TELE1,INVOICE_HEAD.SOLD_TO,INVOICE_HEAD.SOLD_NAME,INVOICE_HEAD.SOLD_ADDR,INVOICE_HEAD.SOLD_ADDR2,INVOICE_HEAD.SOLD_ADDR3,INVOICE_HEAD.SOLD_PIN,
          INVOICE_HEAD.SOLD_CITY,INVOICE_HEAD.SOLD_STATE,INVOICE_HEAD.SOLD_REGION,INVOICE_HEAD.CUSTOMER,INVOICE_HEAD.CUST_NAME,INVOICE_HEAD.FULL_NAME,INVOICE_HEAD.CUST_CODE,INVOICE_HEAD.CUST_ADDR,
          INVOICE_HEAD.CUST_ADDR2,INVOICE_HEAD.CUST_ADDR3,INVOICE_HEAD.CUST_PIN,INVOICE_HEAD.CUST_CITY,INVOICE_HEAD.CUST_STATE,INVOICE_HEAD.CUST_EU_VAT,INVOICE_HEAD.TRANS_MODE,INVOICE_HEAD.CUST_PARITY,
          INVOICE_HEAD.ORDER_NO,INVOICE_HEAD.ORDER_DATE,INVOICE_HEAD.EXPORT_CONTRACT_NO,INVOICE_HEAD. DESP_DATE,INVOICE_HEAD.INVOICE_DATE,INVOICE_HEAD.DUE_DATE,INVOICE_HEAD.RCP_MODE,INVOICE_HEAD.CR_TERM,
          INVOICE_HEAD.TERMS_OF_DELIVERY,INVOICE_HEAD.ITEM_SER,INVOICE_HEAD.EXCH_RATE,INVOICE_HEAD.CURR_CODE,INVOICE_HEAD.NET_AMT,INVOICE_HEAD.TAX_AMT,INVOICE_HEAD.WITHOUT_VAT,INVOICE_HEAD.DESTINATION,
          INVOICE_HEAD.REMARK,INVOICE_HEAD.COMM_INV
      );      
      INSERT_EXPORT_INVOICE_BODY(INVOICE_HEAD.INVOICE_ID);      
    END LOOP;
    CLOSE HEAD_CUR;
    
END;
PROCEDURE INSERT_INVOICES AS
BEGIN
  INSERT_INLAND_INVOCES;
  INSERT_EXPORT_INVOCES;
END;

END;

/
